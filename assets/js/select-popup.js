// JavaScript Document

$(document).ready(function () {
    $('#comment_how_much').mouseenter(function () {
        var popup_id = '#' + $(this).attr('id').replace('comment', 'popup');
        $(popup_id).show();
    });
    $('#comment_how_much').mouseleave(function () {
        var popup_id = '#' + $(this).attr('id').replace('comment', 'popup');
        $(popup_id).hide();
    });

});

function show_hide_steps(hide_step, show_step) {
    var step_map = {
        1: ".step-one",
        2: ".step-two",
        3: ".step-three",
        4: ".step-four",
        5: ".step-five",
        6: ".step-six",
        7: ".step-seven",
        8: ".step-eight",
        9: ".step-nine",
        10: ".step-ten"
    };

    $(step_map[hide_step]).fadeOut(300);
    $(step_map[show_step]).delay(300).fadeIn();

    $('#steps_indicator li[class*="circular-dot-' + hide_step + '"]').removeClass('blue-dot');
    $('#steps_indicator li[class*="circular-dot-' + show_step + '"]').addClass('blue-dot');

    if (show_step == 10) {
        $('#steps_indicator').hide();
    } else {
        if (!$('#steps_indicator').is(':visible')) {
            $('#steps_indicator').show();
        }
    }
}

function select_branching_path(path_mode, path_value) {
    if (path_mode == 'home_owner') {
        if (path_value == 'Yes') {
            $('.circular-dot-8').removeClass('hidden-dot');
            $('.circular-dot-9').removeClass('hidden-dot');

            show_hide_steps(7, 8);
        } else {
            if ($('.circular-dot-8').is(':visible')) {
                $('.circular-dot-8').addClass('hidden-dot');
            }
            if ($('.circular-dot-9').is(':visible')) {
                $('.circular-dot-9').addClass('hidden-dot');
            }

            show_hide_steps(7, 10);
        }
    } else if (path_mode == 'student_loan') {
        if (path_value == 'Yes') {
            $('.circular-dot-4').removeClass('hidden-dot');
            $('.circular-dot-5').removeClass('hidden-dot');

            show_hide_steps(3, 4);
        } else {
            if ($('.circular-dot-4').is(':visible')) {
                $('.circular-dot-4').addClass('hidden-dot');
            }
            if ($('.circular-dot-5').is(':visible')) {
                $('.circular-dot-5').addClass('hidden-dot');
            }

            show_hide_steps(3, 6);
        }
    }
}

function show_result(hide_step, show_step) {
    $("." + hide_step).delay(7000).fadeOut('slow', function () {
        $("." + show_step).fadeIn();
    });
}

function validate_loan_form(step) {
    var my_input = '';
    var how_much = '';
    switch (step) {
        case 1:
            my_input = $("#Unsecured_Debt option:selected").val();
            how_much = parseInt(my_input);
            //alert(my_input);
            if (!my_input) {
                alert('Please select - How much do you owe?');
                return false;
            } else if (my_input == "$5,000" || my_input == "$7,500") {
                alert('Sorry, you must have a minimum of $10,000 in unsecured debt to apply.');
                $("#Unsecured_Debt")[0].selectedIndex = 0;
                return false;
            } else {
                show_hide_steps(1, 2);
            }
            break;
        case 2:
            my_input = $("#step_two option:selected").val();
            if (!my_input) {
                alert('Please select - Are you behind on bills?');
                return false;
            } else {
                show_hide_steps(2, 3);
            }
            break;
        case 3:
            my_input = $("#step_three option:selected").val();
            if (!my_input) {
                alert('Please select - Do you have student loan debt?');
                return false;
            } else {
                select_branching_path('student_loan', my_input);
            }
            break;
        case 4:
            my_input = $("#Student_Loan_Debt option:selected").val();
            how_much = parseInt(my_input);
            if (!my_input) {
                alert('Please select - How much student loan debt do you have?');
                return false;
            } else {
                show_hide_steps(4, 5);
            }
            break;
        case 5:
            my_input = $("#step_five option:selected").val();
            if (!my_input) {
                alert('Please select - How many years has it been since you last attended class?');
                return false;
            } else {
                show_hide_steps(5, 6);
            }
            break;
        case 6:
            my_input = $("#step_six option:selected").val();
            if (!my_input) {
                alert('Please select - What is your credit rating?');
                return false;
            } else {
                show_hide_steps(6, 7);
            }
            break;
        case 7:
            my_input = $("#step_seven option:selected").val();
            if (!my_input) {
                alert('Please select - Are you a home owner?');
                return false;
            } else {
                select_branching_path('home_owner', my_input);
            }
            break;
        case 8:
            my_input = $("#step_eight option:selected").val();
            if (!my_input) {
                alert('Please select - What is your property value?');
                return false;
            } else {
                show_hide_steps(8, 9);
            }
            break;
        case 9:
            step_eight = $("#step_eight option:selected").val();
            if (step_eight.length > 0) {
                step_eight = parseInt(step_eight);
            }
            step_nine = $("#step_nine option:selected").val();
            if (step_nine.length > 0) {
                step_nine = parseInt(step_nine);
            }
            if (!step_nine) {
                alert('Please select - What is your mortgage balance?');
                return false;
            }
            /*
            else if(step_five <= step_six)
            {
            	alert('Property value MUST be a larger number than mortgage balance.');
            	$('#step_five').get(0).selectedIndex = 0;
            	$('#step_six').get(0).selectedIndex = 0;
            	show_hide_steps('step-six', 'step-five');
            	return false;
            }
            */
            else {
                show_hide_steps(9, 10);
            }
            break;
        case 10:
            my_input = $("#postal_code").val();
            //alert(my_input);
            if (!my_input) {
                alert('Please enter your Postal Code');
                $('#postal_code').focus();
                return false;
            } else if (my_input.length != 6) {
                alert('Please enter a valid 6-digit Canadian postal code (no space)');
                $('#postal_code').focus();
                return false;
            } else if (my_input.indexOf(" ") != -1) {
                alert('Please enter a valid canadian postal code (no space)');
                $('#postal_code').focus();
                return false;
            } else {
                lookup_city_province();
                return false;
            }
        default:
    }

    return true;
}

function control_postal_bg() {
    postal_code = $("#postal_code").val();
    postal_code = $.trim(postal_code);
    $("#postal_code").val(postal_code);
    if (postal_code.length) {
        $("#postal_code").css("background-image", "none");
    } else {
        $("#postal_code").css("background-image", "../css/images/result-bg.png");
    }
}

function lookup_city_province() {
    //$('#loan_form').attr('action', '/p2/qualify.php');
    var qualify_page = '/debt/qualify.php';
    $('#loan_form').attr('action', qualify_page);
    $('#loan_form').submit();

    /*
    	var data = jQuery("#loan_form").serializeArray();
    	var postal_code = jQuery("#postal_code").val();
    	
    	jQuery.ajax({
    		type: "POST",
    		url: 'find_city.php',
    		data: data,
    		dataType: "json",
    		timeout: 5000, // sets timeout to 5 seconds
    		cache: false,
    		success: function(data){
    			
    			$('#loan_form').attr('action', '/p2/qualify.php');
    			$('#loan_form').submit();
    		},
    		error: function(request, status, error){ 
    			setTimeout(lookup_city_province, 1000);
    		}
    	});
    */
}

function jump_to_main_form(id) {
    $('html, body').animate({
        scrollTop: $("#" + id).offset().top
    }, 1000);

    return false;
}
